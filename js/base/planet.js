import Vector2 from "./vector2.js";

const G = 6.6742 * 10**-11
// const G = 66.742

const DISTANCE_COEF = 10**5.7


export default class Planet {

    constructor(name, position, direction, mass, size, color) {
        this.name = name;
        this.position = position;
        this.direction = direction;
        this.mass = mass;
        this.size = size;
        this.color = color;
        this.dead = false;
    }

    update(ctx, others) {
        if (!this.dead) {
            this.attraction(others);
            this.applyForce(this.direction);
            this.show(ctx);
        }
    }

    attraction(others) {
        for (let planet of others) {
            if (planet !== this)
            {
                let massDifferenceCoef = this.mass / planet.mass;
                let force = (G * this.mass * planet.mass) / (this.position.dist(planet.position)*DISTANCE_COEF)**2;

                if (massDifferenceCoef >= 1)
                {
                    force *= 1/massDifferenceCoef;
                }

                let orientation = new Vector2(planet.position.x - this.position.x , planet.position.y - this.position.y);
                orientation = orientation.normalize();
                orientation = orientation.mult(force);
                this.direction = this.direction.add(orientation.x, orientation.y);

                this.collide(planet);
            }
        }
    }

    applyForce(force) {
        this.position.set(new Vector2(this.position.x + force.x, this.position.y + force.y));
    }

    collide(other) {
        if (this.position.dist(other.position) <= this.size + other.size) {
            if (this.mass >= other.mass) {
                this.mass += other.mass;
                other.dead = true;
            }
        }
    }

    show(ctx) {
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.arc(this.position.x, this.position.y, this.size, 0, 360);
        ctx.fill();
    }
}
