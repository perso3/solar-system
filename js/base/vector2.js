
export default class Vector2 {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    set(v) {
        this.x = v.x;
        this.y = v.y;
    }

    add(x, y) {
        return new Vector2(this.x + x, this.y + y);
    }

    sub(x, y) {
        return new Vector2(this.x - x, this.y - y);
    }

    mult(v) {
        return new Vector2(this.x * v, this.y * v);
    }

    div(v) {
        return new Vector2(this.x / v, this.y / v);
    }

    length() {
        return Math.sqrt(this.x ** 2 + this.y ** 2);
    }

    normalize(self) {
        return this.div(this.length());
    }

    dist(other) {
        return Math.sqrt((this.x - other.x) ** 2 + (this.y - other.y) ** 2);
    }

}