import Planet from "./base/planet.js"
import Vector2 from "./base/vector2.js";

"use strict";

let canvas;
let canvasSize;
let context;

window.onload = init;

function init(){
    canvas = document.getElementById('canvas');
    canvas.height = innerHeight;
    canvas.width = innerWidth;
    canvasSize = new Vector2(canvas.width, canvas.height);
    context = canvas.getContext('2d');
    // Start the first frame request
    window.requestAnimationFrame(gameLoop);
}

let secondsPassed;
let oldTimeStamp;
let displayedFps;
let gameLogicSpeed = 60;

let planet1 = new Planet("Earth", new Vector2(450, 400), new Vector2(0, 0), 5.972 * 10**24, 20, "#ff0000")
let planet2 = new Planet("Comet A", new Vector2(200, 400), new Vector2(4, 4), 5, 20, "#0033ff")
let planet3 = new Planet("Comet B", new Vector2(650, 400), new Vector2(-4, -4), 50, 20, "#0033ff")
let planets = [planet1, planet2, planet3]

function gameLoop(timeStamp) {

    // Calculate the number of seconds passed since the last frame
    secondsPassed = (timeStamp - oldTimeStamp) / 1000;
    oldTimeStamp = timeStamp;

    // Calculate displayed fps
    displayedFps = Math.round(1 / secondsPassed);

    context.fillStyle = 'white';
    context.clearRect(0, 0, canvasSize.x, canvasSize.y);

    // Perform the drawing operation
    draw();

    // Draw number to the screen
    context.font = '25px Arial';
    context.fillStyle = 'black';
    context.fillText("FPS: " + displayedFps, 10, 30);

    // The loop function has reached it's end. Keep requesting new frames
    window.requestAnimationFrame(gameLoop);
}

function draw(){
    let deaths = []
    for (const planet of planets) {
        console.log(planet.name, planet.position);
        planet.update(context, planets);
        if (planet.dead){
            deaths.push(planet);
        }
    }

    for (const death of deaths) {
        let index = planets.indexOf(death);
        planets.splice(index, 1);
    }

}